package main // import "gitlab.com/tjgillies/talker"



import (
	"bufio"
	"bytes"
	"crypto"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"time"

	"golang.org/x/crypto/openpgp"
	"golang.org/x/crypto/openpgp/armor"
	"golang.org/x/crypto/openpgp/packet"
	"golang.org/x/crypto/ssh/terminal"
	"gopkg.in/src-d/go-git.v4"
	"gopkg.in/src-d/go-git.v4/plumbing/object"
)

// func sign(msg string) {
// 	var buf bytes.Buffer
// 	ent := openpgp.Entity{PrimaryKey: }
// 	openpgp.ArmoredDetachSignText(buf, )
// }

func getInput() string {
	reader := bufio.NewReader(os.Stdin)
	fmt.Print("Enter text: ")
	text, _ := reader.ReadString('\n')
	return text
}

func gitLoop(w *git.Worktree, fe *openpgp.Entity, r *git.Repository, re *git.Remote, mk string) {
	//err := w.Pull(&git.PullOptions{})
	//log.Println(err)

	c, err := w.BuildCommit(mk, &git.CommitOptions{
		Author: &object.Signature{
			Name: "Tyler", Email: "tjgillies@gmail.com",
			When: time.Now(),
		},
		Committer: &object.Signature{
			Name: "Tyler", Email: "tjgillies@gmail.com",
			When: time.Now(),
		},
	})

	if err != nil {
		log.Println(err)
	}

	var b bytes.Buffer

	m, _ := c.EncodeF()
	out, _ := ioutil.ReadAll(m)
	//log.Println(string(out))
	msg := bytes.NewReader(out)
	openpgp.ArmoredDetachSign(&b, fe, msg, &packet.Config{
		DefaultHash: crypto.SHA256,
	})
	sig := b.String()
	//log.Println(err)
	c.PGPSignature = sig
	_, err = w.SaveCommit(c)
	if err != nil {
		log.Println(err)
	}

	//log.Println(err)
	err = re.Push(&git.PushOptions{RemoteName: "origin"})
	//log.Println(h)
	if err != nil {
		log.Println(err)
	}

}

func main() {
	r, err := git.PlainOpen(".")
	log.Println(err)
	w, err := r.Worktree()
	log.Println(err)
	re, err := r.Remote("origin")
	log.Println(err)

	from, err := os.Open("key.asc")
	log.Println(err)
	foo, _ := ioutil.ReadAll(from)
	//log.Println(foo)
	from2 := bytes.NewReader(foo)
	fromBlock, err := armor.Decode(from2)
	log.Println(err)
	fromReader := packet.NewReader(fromBlock.Body)
	fromEntity, err := openpgp.ReadEntity(fromReader)
	log.Println("Enter Key Passphrase:")
	pw, err := terminal.ReadPassword(0)
	fromEntity.PrivateKey.Decrypt(pw)

	go func() {
		for {
			msg := getInput()
			gitLoop(w, fromEntity, r, re, msg)
		}
	}()

	//gitLoop(w, r, re, "test")

	for {
		he, _ := r.Head()
		time.Sleep(time.Second * 5)
		err := w.Pull(&git.PullOptions{})
		if err != nil {
			//log.Println(err)
		}
		he2, _ := r.Head()
		it, err := r.Log(&git.LogOptions{
			From: he2.Hash(),
		})
		if err != nil {
			log.Println(err)
		}
		for {
			co, err := it.Next()
			if err != nil {
				//log.Println(err)
				break
			}
			if co.Hash == he.Hash() {
				break
			}
			log.Println(co)


		}
		//it.ForEach(func(co *object.Commit) error {
		//	if he.Hash() != he2.Hash() {
		//		log.Println(co)
		//	}
		//	return nil
		//})
		//if err != nil {
		//	log.Println(err)
		//}

	}

}
